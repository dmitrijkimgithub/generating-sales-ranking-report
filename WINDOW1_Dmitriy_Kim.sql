/* 1. Common Table Expression (CTE): RankedCustomers:
 *    - The CTE calculates the total sales for each customer in each channel for the specified years (1998, 1999, and 2001).
 *    - It uses the ROW_NUMBER() window function to assign a unique rank to each customer within their channel and sales year, 
 *      ordered by total sales in descending order.
 *    - The CTE includes columns such as cust_id, cust_first_name, cust_last_name, channel_id, sales_year, total_sales, and sales_rank.
 * 2. Main Query:
 *    - The main query selects information from the RankedCustomers CTE and joins it with the channels table to include the channel description.
 *    - It includes conditions to filter data:
 *    - Customers who are ranked in the top 300 (WHERE rc.sales_rank <= 300).
 *    - Purchases made in the "Internet" channel (AND ch.channel_desc = 'Internet').
 *    - The result includes columns like cust_id, cust_first_name, cust_last_name, channel_id, sales_year, total_sales, sales_rank, and channel_desc.
 *    - The final result set is ordered by sales year and sales rank.
 *    In summary, the query generates a sales report for customers ranked in the top 300 based on total sales in the years 1998, 1999, and 2001, 
 *    categorized by the "Internet" channel. The report includes customer details, sales information, and channel descriptions.
 */

WITH RankedCustomers AS (
    SELECT
        c.cust_id,
        c.cust_first_name,
        c.cust_last_name,
        s.channel_id,
        EXTRACT(YEAR FROM t.time_id) AS sales_year,
        SUM(s.amount_sold) AS total_sales,
        ROW_NUMBER() OVER (PARTITION BY s.channel_id, EXTRACT(YEAR FROM t.time_id) ORDER BY SUM(s.amount_sold) DESC) AS sales_rank
    FROM
        customers c
        JOIN sales s ON c.cust_id = s.cust_id
        JOIN times t ON s.time_id = t.time_id
        JOIN channels ch ON s.channel_id = ch.channel_id
    WHERE
        EXTRACT(YEAR FROM t.time_id) IN (1998, 1999, 2001)
        AND ch.channel_desc = 'Internet'
    GROUP BY
        c.cust_id,
        c.cust_first_name,
        c.cust_last_name,
        s.channel_id,
        EXTRACT(YEAR FROM t.time_id)
)

SELECT
    rc.cust_id,
    rc.cust_first_name,
    rc.cust_last_name,
    rc.channel_id,
    rc.sales_year,
    rc.total_sales,
    rc.sales_rank,
    c.channel_desc
FROM
    RankedCustomers rc
    JOIN channels c ON rc.channel_id = c.channel_id
WHERE
    rc.sales_rank <= 300
ORDER BY
    rc.sales_year,
    rc.sales_rank;

